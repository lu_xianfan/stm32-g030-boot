#include "bsp.h"

#define HEAD 0X55
#define ID_L 0XFE
#define ID_H 0XFE


typedef struct
{
	uint8_t RxBuf[8];
	uint8_t RxCount;
}MSG_T;

MSG_T g_Msg;

static uint8_t ReadCommand(uint8_t _Address,uint8_t _Message);/*读命令*/
static uint8_t WriteCommand(uint8_t _Address,uint8_t _Message);	 /*写命令*/
static uint8_t ControlCommand(uint8_t _Address,uint8_t _Message);/*控制命令 */
//static void ReportCommand(void);/**/

/*要保证这个函数里执行的命令是非阻塞的..*/

/*  
	printf("打开 - 55 FE FE 03 01 B9 24\r\n");
	printf("关闭 - 55 FE FE 03 02 F9 25\r\n");
	printf("停止 - 55 FE FE 03 03 38 E5\r\n");
*/
void form_MsgPoll(void)
{
	uint16_t i;
	uint16_t s_RxCount;
	uint8_t Msg[8];
	
	uint8_t s_Command;
	uint8_t s_Address;
	uint8_t s_Message;
	
	uint16_t s_CRC;
	
	if(comReturnIdleFlag(COMMSG) == 0)
	{
		return;
	}
	/*空闲中断触发后，接收一个包的数据*/
	s_RxCount = comGetRxCount(COMMSG);
	
	if(s_RxCount != 0x07 && s_RxCount != 0x08)/*包长度校验*/
	{
		goto  fail;
	}
	for(i = 0; i < s_RxCount ;i++)
	{
		comGetChar(COMMSG,&Msg[i]);
	}
	
	if(CRC16_Modbus(Msg,s_RxCount) != 0)/*CRC校验*/
	{
		s_CRC = CRC16_Modbus(Msg,s_RxCount-2);
		Msg[s_RxCount-2] = s_CRC >> 8;
		Msg[s_RxCount-1] = s_CRC;
		////DEBUG_LOG("CRC:%x %x\r\n",Msg[s_RxCount-2],Msg[s_RxCount-1]);
		comSendBuf(COMMSG,Msg,s_RxCount);/*回复正确的CRC*/
		goto fail;
	}
	if(Msg[0] != HEAD || Msg[1] != ID_L || Msg[2] != ID_H)/*包头,ID校验*/
	{
		goto fail;
	}
	
	s_Command = Msg[3];
	s_Address = Msg[4];
	s_Message = Msg[5];
	
	switch(s_Command)
	{
		case 0x01:
			ReadCommand(s_Address,s_Message);
			break;
		
		case 0x02:
			WriteCommand(s_Address,s_Message);
			break;
		
		case 0x03:
			ControlCommand(s_Address,s_Message);
			break;
		
		case 0x04:
			
			break;
		
		default:
			
			goto fail;
			break;
	}
	
	
	
	fail:
	comClearRxFifo(COMMSG);  /* 清除缓存 */
	comClearIdleFlag(COMMSG);/* MSG处理后清除空闲标志 */
}

static uint8_t ReadCommand(uint8_t _Address,uint8_t _Message)/*读命令*/
{
	uint16_t s_CRC;
	uint8_t s_MsgReturnBuf[9];
	if(_Address == 0x02 && _Message == 0x01)//读位置
	{
		s_MsgReturnBuf[0] = HEAD;
		s_MsgReturnBuf[1] = ID_L;
		s_MsgReturnBuf[2] = ID_H;
		s_MsgReturnBuf[3] = 0x01;
		s_MsgReturnBuf[4] = _Address;
		s_MsgReturnBuf[5] = 0X01;
		if(g_CheckFlag == 0x01)
		{
			s_MsgReturnBuf[6] = form_ReadPosition();
		}
		else if(g_CheckFlag == 0x00)
		{
			s_MsgReturnBuf[6] = 0xFF;
		}
		s_CRC = CRC16_Modbus(s_MsgReturnBuf,7);
		s_MsgReturnBuf[7] = s_CRC >> 8;
		s_MsgReturnBuf[8] = s_CRC;
		comSendBuf(COMMSG,s_MsgReturnBuf,9);
	}	
}
static uint8_t WriteCommand(uint8_t _Address,uint8_t _Message) /*写命令*/
{
	uint16_t s_CRC;
	if(_Address == 0x01)//设置方向
	{
		//form_CurtainOpen();
		//////DEBUG_LOG("窗帘打开\r\n");
	}
	else if(_Address == 0x02)//设置手拉使能
	{
		//form_CurtainClose();
		//////DEBUG_LOG("窗帘关闭\r\n");
	}
	else
	{
		return 0;
	}
}

/*回复函数*/
static void ReturnCommend(uint8_t _Address,uint8_t _Message,uint8_t _CheckFlag)
{
	uint8_t  s_MsgReturnBuf[9];
	uint16_t s_CRC;
	if(_Address == 0x01 || _Address == 0x02 || _Address == 0x03)
	{
		s_MsgReturnBuf[0] = HEAD;
		s_MsgReturnBuf[1] = ID_L;
		s_MsgReturnBuf[2] = ID_H;
		s_MsgReturnBuf[3] = 0x03;
		s_MsgReturnBuf[4] = _Address;
		s_CRC = CRC16_Modbus(s_MsgReturnBuf,5);
		s_MsgReturnBuf[5] = s_CRC >> 8;
		s_MsgReturnBuf[6] = s_CRC;
		comSendBuf(COMMSG,s_MsgReturnBuf,7);
	}
	if(_Address == 0x04)
	{
		s_MsgReturnBuf[0] = HEAD;
		s_MsgReturnBuf[1] = ID_L;
		s_MsgReturnBuf[2] = ID_H;
		s_MsgReturnBuf[3] = 0x03;
		s_MsgReturnBuf[4] = _Address;
		if(_CheckFlag == 0x01 && _Message <= 100)
		{
			s_MsgReturnBuf[5] = _Message;
		}
		else
		{
			s_MsgReturnBuf[5] = 0xFF;
		}
		
		s_CRC = CRC16_Modbus(s_MsgReturnBuf,6);
		s_MsgReturnBuf[6] = s_CRC >> 8;
		s_MsgReturnBuf[7] = s_CRC;
		comSendBuf(COMMSG,s_MsgReturnBuf,7);		
	}
}
/*
返回0 命令失败
返回1 命令成功
*/
//static uint8_t g_MsgReturnBuf[8] = {0x55,0xFE,0xFE,0x00,0x00,0x00,0x00,0x00};
static uint8_t ControlCommand(uint8_t _Address,uint8_t _Message)/*控制命令 */
{
	uint16_t s_CRC;
	uint8_t s_CheckFlag;
	s_CheckFlag = g_CheckFlag;
	if(_Address == 0x01)//打开
	{
		if(DCMotor[0].State != 0x01)
		{
			DCMotor[0].Speed = 10000;
			form_CurtainOpen();
			//////DEBUG_LOG("窗帘打开（已设行程）\r\n");	
		}
	}
	else if(_Address == 0x02)//关闭
	{
		if(DCMotor[0].State != 0x02)
		{
			DCMotor[0].Speed = 10000;
			form_CurtainClose();
			//////DEBUG_LOG("窗帘关闭（已设行程）\r\n");
		}
	}
	else if(_Address == 0x03)//停止
	{
		form_Stop();
		//////DEBUG_LOG("停止\r\n");
	}
	else if(_Address == 0x04)//百分比控制
	{
		if(g_CheckFlag == 1 && _Message <= 100 && DCMotor[0].State == 0x00)
		{
			form_CurtainLevel(_Message);
			////DEBUG_LOG("百分比控制\r\n");	
		}
	}
	else if(_Address == 0x05)//上边界设置
	{
		form_SetUpstroke(_Message);
	}
	else if(_Address == 0x06)//下边界设置
	{
		form_SetDownstroke(_Message);
	}
	else
	{
		/*不是已知的命令，不回复*/
		return 0;
	}
	/*是已知的命令，回复*/
	ReturnCommend(_Address,_Message,s_CheckFlag);
	return 1;
	
}

void returnPos(void)
{
	uint16_t s_CRC;
	uint8_t s_MsgReturnBuf[16];
	s_MsgReturnBuf[0] = HEAD;
	s_MsgReturnBuf[1] = ID_L;
	s_MsgReturnBuf[2] = ID_H;
	s_MsgReturnBuf[3] = 0x04;
	s_MsgReturnBuf[4] = 0x02;
	s_MsgReturnBuf[5] = 0X08;
	
	//电机当前位置
	if(g_CheckFlag == 0x01)
	{
		s_MsgReturnBuf[6] = form_ReadPosition();
	}
	else if(g_CheckFlag == 0x00)
	{
		s_MsgReturnBuf[6] = 0xFF;
	}
	s_MsgReturnBuf[7] = DCMotor[0].Dir;//电机方向
	s_MsgReturnBuf[8] = 0x01;
	s_MsgReturnBuf[9] = DCMotor[0].State;//电机状态
	s_MsgReturnBuf[10] = 0x00;
	s_MsgReturnBuf[11] = 0x00;
	s_MsgReturnBuf[12] = 0x00;
	s_MsgReturnBuf[13] = g_CheckFlag;

	s_CRC = CRC16_Modbus(s_MsgReturnBuf,14);
	s_MsgReturnBuf[14] = s_CRC >> 8;
	s_MsgReturnBuf[15] = s_CRC;
	comSendBuf(COMMSG,s_MsgReturnBuf,16);
}