#ifndef __FORM_CURTAINCONTROL_H
#define __FORM_CURTAINCONTROL_H

uint8_t form_ReadPosition(void);
void form_CurtainOpen(void);
void form_CurtainClose(void);
void form_CurtainLevel(uint8_t _Level);
void form_SetUpstroke(uint8_t _State);
void form_SetDownstroke(uint8_t _State);
void form_Stop(void);
#endif
