/*
*********************************************************************************************************
*
*	模块名称 : 增量式编码器模块
*	文件名称 : bsp_Encoder.c
*	版    本 : V1.0
*	说    明 : 增量式编码器模块
*
*	修改记录 :
*		版本号  日期        		作者     		说明
*		V1.0    2021-08-25  	FlyyingPiggy  	正式发布
*
*********************************************************************************************************
*/
#include "bsp.h"

uint8_t  g_flag = 0;//0正转
GPIO_PinState PA5;
GPIO_PinState PA4;


void bsp_InitEncoder(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	//__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/*Configure GPIO pins : PA4 PA5 */
	GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
}


void EXTI4_15_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI4_15_IRQn 0 */

	/* USER CODE END EXTI4_15_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
	/* USER CODE BEGIN EXTI4_15_IRQn 1 */

	/* USER CODE END EXTI4_15_IRQn 1 */
}



void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
	
	switch(GPIO_Pin)
	{
		case GPIO_PIN_4:
			PA5 = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_5);
			if(PA5 == GPIO_PIN_SET)
			{
				g_flag = 0;
			}
			else if(PA5 == GPIO_PIN_RESET)
			{
				g_flag = 1;
			}
			break;
		case GPIO_PIN_5:
			PA4 = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_4);
			if(PA4 == GPIO_PIN_SET)
			{
				g_flag = 1;
			}
			else if(PA4 == GPIO_PIN_RESET)
			{
				g_flag = 0;
			}
			break;
	}
	if(g_flag == 1)
	{
		DCMotor[0].Count = DCMotor[0].Count - 1; 
	}
	else if(g_flag == 0)
	{
		DCMotor[0].Count = DCMotor[0].Count + 1;
	}
}

void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
		case GPIO_PIN_4:
			PA5 = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_5);
			if(PA5 == GPIO_PIN_SET)
			{
				g_flag = 1;
			}
			else if(PA5 == GPIO_PIN_RESET)
			{
				g_flag = 0;
			}
			break;
		case GPIO_PIN_5:
			PA4 = HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_4);
			if(PA4 == GPIO_PIN_SET)
			{
				g_flag = 0;
			}
			else if(PA4 == GPIO_PIN_RESET)
			{
				g_flag = 1;
			}
			break;
	}	
	if(g_flag == 1)
	{
		DCMotor[0].Count = DCMotor[0].Count - 1; 
	}
	else if(g_flag == 0)
	{
		DCMotor[0].Count = DCMotor[0].Count + 1;
	}
}
