#ifndef __BSP_DCBRUSHEDMOTORS_H
#define __BSP_DCBRUSHEDMOTORS_H


#define DCM_COUNT 1
/*直流电机参数*/
typedef struct
{
	volatile uint8_t  Dir;			/* 直流电机方向 0正转 1反转*/
	volatile uint16_t Speed;		/* 直流电机速度 0*/
	volatile uint8_t  State;		/* 直流电机状态 0停止 1打开 2关闭*/
	volatile int16_t  Count;		/* 当前行程 0*/
	volatile int16_t  UpCount;		/* 上行程 0*/
	volatile int16_t  DownCount;	/* 下行程 0*/
	volatile int16_t  Position;		/* 目标行程 0*/
	volatile uint8_t  PostionFlag;	/* 标志位*/
	volatile uint8_t  UpSetFlag;	/* 上行程已设置标志 */
	volatile uint8_t  DownSetFlag;  /* 下行程已设置标志*/
}DCMOROT_ATR;

extern DCMOROT_ATR DCMotor[DCM_COUNT];
void bsp_InitDcMotor(void);
void bsp_set_DCMotor_speed(uint8_t _id,uint16_t v);
void bsp_set_DCMotor_direction(uint8_t _id,uint8_t dir);
void bsp_set_DCMotor_enable(uint8_t _id);
void bsp_set_DCMotor_disable(uint8_t _id);

#endif
