#include "bsp.h"

/*
address					    size	page	
0x0800F800 - 0x0800FFFF	 	2K		page31
*/
uint32_t FlashSaveData[];
#define STM32_FLASH_BASE 0x08000000
#define FLASH_WAITETIME	10
uint32_t STMFLASH_ReadWord(uint32_t faddr)
{
	return *(volatile uint32_t*)faddr;
}

/*��Flash�ڲ�д��*/
void STMFLASH_Write(uint32_t WriteAddr, uint32_t *pBuffer,uint32_t NumToWrite)
{
	FLASH_EraseInitTypeDef FlashEraseInit;
	HAL_StatusTypeDef FlashStatus = HAL_OK;
	
	uint32_t PageError = 0;
	uint32_t addrx = 0;
	uint32_t endaddr = 0;
	
	
	
	
	addrx = WriteAddr;
	endaddr = WriteAddr + NumToWrite;
	HAL_FLASH_Unlock();
	if(addrx < 0x1FFF0000)
	{
		while(addrx < endaddr)
		{
			if(STMFLASH_ReadWord(addrx) != 0xFFFFFFFF)
			{
				FlashEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
				FlashEraseInit.Page = 15;
				FlashEraseInit.NbPages = 1;
				if(HAL_FLASHEx_Erase(&FlashEraseInit,&PageError) != HAL_OK)
				{
					break;
				}
			}
			else
			{
				addrx += 4;
			}
			FLASH_WaitForLastOperation(FLASH_WAITETIME);
		}
	}
	
	FlashStatus = FLASH_WaitForLastOperation(FLASH_WAITETIME);
	if(FlashStatus == HAL_OK)
	{
		while(WriteAddr < endaddr)
		{
			if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,WriteAddr,*(uint64_t*)pBuffer) != HAL_OK)
			{
				break;
			}
			WriteAddr += 8;
			pBuffer += 2;
		}
	}
	HAL_FLASH_Lock();
}

void STMFLASH_Read(uint32_t ReadAddr, uint32_t *pBuffer, uint32_t NumToRead)
{
	uint32_t i;
	for(i = 0; i< NumToRead; i++)
	{
		pBuffer[i] = STMFLASH_ReadWord(ReadAddr);
		ReadAddr += 4;
	}
}