/*
*********************************************************************************************************
*
*	模块名称 : LED指示灯驱动模块
*	文件名称 : bsp_led.c
*	版    本 : V1.0
*	说    明 : 驱动GPIO
*
*	修改记录 :
*		版本号  日期         作者     		说明
*		V1.0    2021-09-05   FlyyingPiggy  	移植LED的亮、灭、翻转(有时间再去移植电平反转。)
*		V1.1	2021-09-14	 FlyyingPiggy	增加LED闪烁异步方式的函数
*
*
*********************************************************************************************************
*/

#include "bsp.h"
/* LED闪烁完成后的回调函数 */
//static void (*s_LEDFLASH_CallBack)(void);

/* 定时器结构体，成员变量必须是 volatile, 否则C编译器优化时可能有问题 */
typedef struct
{
	volatile uint8_t  i;
	volatile uint8_t  Flag;
	volatile uint32_t Count;	/* 计数器 */
	volatile uint32_t PreLoad;	/* 计数器预装值 */
	void (*_CallBack)(void);
}LED_FLASH;


LED_FLASH LedFlash;

static volatile uint32_t s_FlashTimes = 0;/* 闪烁次数 */
/*
*********************************************************************************************************
*	函 数 名: bsp_InitLed
*	功能说明: 配置LED指示灯相关的GPIO,  该函数被 bsp_Init() 调用。
*	形    参:  无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitLed(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,GPIO_PIN_RESET);
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedOn
*	功能说明: 点亮指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_LedOn(uint8_t _no)
{
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedOff
*	功能说明: 熄灭指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_LedOff(uint8_t _no)
{
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedToggle
*	功能说明: 翻转指定的LED指示灯。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 按键代码
*********************************************************************************************************
*/
void bsp_LedToggle(uint8_t _no)
{
	HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_0);
}

/*
*********************************************************************************************************
*	函 数 名: bsp_LedFlash
*	功能说明: 异步方式闪烁LED灯
*	形    参:  _no : 指示灯序号，范围 1 - 4(目前只有一个灯，可以随便填)
*			  _Times : 闪烁次数 
*			  _Frequency : 闪烁频率 HZ
*	返 回 值: NULL
*********************************************************************************************************
*/
void bsp_LedFlash(uint8_t _Times ,uint16_t _Frequency,void *_CallBack)
{
	LedFlash.i = 0;
	LedFlash.Count = 0;
	LedFlash.PreLoad = _Times * 2;/* 一亮一灭，才是一次闪烁*/
	LedFlash.Flag = 0;
	LedFlash._CallBack = (void (*)(void))_CallBack;/* 闪烁完毕后执行的函数 */
	bsp_StartAutoTimer(5, _Frequency);
}
/*检测闪烁是否完成*/
uint8_t bsp_CheckLedFlash(uint8_t _no)
{
	if(LedFlash.Flag == 1)
	{
		return 1;
	}
	return 0;
}
/**/
void bsp_LedFlashPoll(void)
{
	if(bsp_CheckTimer(5))
	{
		
		if(LedFlash.Count != LedFlash.PreLoad)
		{
			bsp_LedToggle(0);
			LedFlash.Count++;
		}
		else
		{
			bsp_StopTimer(5);
			LedFlash.Flag = 1;
			if(LedFlash._CallBack != NULL)
			{
				LedFlash._CallBack();				
			}
		}
	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_IsLedOn
*	功能说明: 判断LED指示灯是否已经点亮。
*	形    参:  _no : 指示灯序号，范围 1 - 4
*	返 回 值: 1表示已经点亮，0表示未点亮
*********************************************************************************************************
*/
uint8_t bsp_IsLedOn(uint8_t _no)
{
	//HAL_GPIO
	return 0;
}

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
