#include "bsp.h"

/* 定义例程名和例程发布日期 */
#define EXAMPLE_NAME	"STM32G030-NF-03"
#define EXAMPLE_DATE	"2021-12-29"
#define DEMO_VER		"1.0"

static void PrintfLogo(void);
static void PrintfHelp(void);
static void nRF24L01_DataUpdate(void);

/* 变量 */
static uint8_t s_ucRxStatus;      
static uint8_t s_ucTxRxMode = 1; /* 0：表示nRF24L01工作于接收状态，1：表示nRF24L01工作于发送状态 */
static uint8_t s_ucTempBuff[32]; /* 数据接收或者发送缓冲 */
static int16_t usAutoPeriod;	 /* 自动定时发送的时间间隔 */

int main(void)
{
	
	uint8_t ucKeyCode, i;		/* 按键代码 */
	uint8_t count= 0;
	bsp_Init();
	PrintfLogo();
	PrintfHelp();
    bsp_DelayMS(1000);
    
    
    
    if(nRF24L01_ConnectCheck() == 1)
	{
		printf("nRF24L01 已连接\r\n");	
	}
	else
	{
		printf("nRF24L01 未连接\r\n");
	}
    
    if(s_ucTxRxMode == 0)
    {
        nRF24L01_RxMode();
        printf("nRF24L01工作于接收状态\r\n");
    }
    else
    {
        nRF24L01_TxMode();
        bsp_StartTimer(0, 1000);
        printf("nRF24L01工作于发送状态\r\n");
    }
	while(1)
	{	
		bsp_Idle();

        if (bsp_CheckTimer(0))	/* 判断定时器超时时间 */
		{
			bsp_StartTimer(0, 1000);	/* 定时周期，单位 ms */
			
			/* 数据发送，每次发送32个字节 */
			for(i = 0; i < TX_PLOAD_WIDTH; i++)
			{
				s_ucTempBuff[i] = count++;
			}
			nRF24L01_TxData(s_ucTempBuff);
			
			/* 更新发送端数据 */
			nRF24L01_DataUpdate();
		}
        
		/* 如果nRF24L01工作于接收模式，进入如下程序 */
		if(s_ucTxRxMode == 0)
		{
			s_ucRxStatus = nRF24L01_RxData(s_ucTempBuff);

			/* 成功接收数据，将数据更新到TFT屏上 */
			if(s_ucRxStatus == RX_DR)
			{	
				nRF24L01_DataUpdate();	
			} 	
		}
        
//        if(nRF24L01_IRQ_Read() != 0)
//        {
//            printf("读取到高电平\r\n");
//        }
//        else
//        {
//            printf("读取到低电平\r\n");
//        }
	}
}


/*
*********************************************************************************************************
*	函 数 名: nRF24L01_DataUpdate
*	功能说明: 32字节数据更新
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void nRF24L01_DataUpdate(void)
{
	printf("buf[0-7] = %d %d %d %d %d %d %d %d\r\n",
    s_ucTempBuff[0],s_ucTempBuff[1],s_ucTempBuff[2],s_ucTempBuff[3],
	s_ucTempBuff[4],s_ucTempBuff[5],s_ucTempBuff[6],s_ucTempBuff[7]
    );
    
    printf("buf[8-15] = %d %d %d %d %d %d %d %d\r\n",
	s_ucTempBuff[8],s_ucTempBuff[9],s_ucTempBuff[10],s_ucTempBuff[11],
	s_ucTempBuff[12],s_ucTempBuff[13],s_ucTempBuff[14],s_ucTempBuff[15]
    );
    
    printf("buf[16-23] = %d %d %d %d %d %d %d %d\r\n",
	s_ucTempBuff[16],s_ucTempBuff[17],s_ucTempBuff[18],s_ucTempBuff[19],
	s_ucTempBuff[20],s_ucTempBuff[21],s_ucTempBuff[22],s_ucTempBuff[23]
    );
    
    printf("buf[24-31] = %d %d %d %d %d %d %d %d\r\n",
	s_ucTempBuff[24],s_ucTempBuff[25],s_ucTempBuff[26],s_ucTempBuff[27],
	s_ucTempBuff[28],s_ucTempBuff[29],s_ucTempBuff[30],s_ucTempBuff[31]
    );
	
}



/*
*********************************************************************************************************
*	函 数 名: PrintfHelp
*	功能说明: 打印操作提示
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void PrintfHelp(void)
{
//	printf("操作提示:\r\n");
//	printf("1. 两条指令发送间隔需要大于20ms。\r\n"); 
//	printf("2. 每1000ms上报一次位置信息。\r\n");
//	printf("控制命令: 0X03\r\n");
//	printf("打开 - 55 FE FE 03 01 B9 24\r\n");
//	printf("关闭 - 55 FE FE 03 02 F9 25\r\n");
//	printf("停止 - 55 FE FE 03 03 38 E5\r\n");
}

/*
*********************************************************************************************************
*	函 数 名: PrintfLogo
*	功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开PC机的超级终端软件可以观察结果
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void PrintfLogo(void)
{
	printf("*************************************************************\n\r");	
	/* 检测CPU ID */
	{
		printf("\r\nCPU : STM32G030F6PF, SOP-20, 主频: %dMHz\r\n ,Flash:32KB ,RAM:8KB ", SystemCoreClock / 1000000);
	}

	printf("\n\r");
	printf("*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印例程名称 */
	printf("* 例程版本   : %s\r\n", DEMO_VER);		/* 打印例程版本 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印例程日期 */
	printf("*************************************************************\n\r");
}
